﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class transpose : MonoBehaviour {

    /*OPCION 2
    public Vector3 targetPos;*/

    [SerializeField]
    private Transform originalTransform;

    [SerializeField]
    private Transform targetTransform; 

	// Use this for initialization
	void Start () {
        transform.position = originalTransform.position; 
	}
	
	// Update is called once per frame
	void Update () {
        /*OPCION 1
        transform.Translate(Vector3.right * Time.deltaTime); */

        /*OPCION 2
        //interpolación de a a b
        transform.position = Vector3.Lerp(transform.position, targetPos, Time.deltaTime);*/

        transform.position = Vector3.Lerp(transform.position, targetTransform.position, Time.deltaTime);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetTransform.rotation, Time.deltaTime); 
        //la flechita de Z te indica hacia dónde está volteando el objeto 

	}

    /*OPCION 3
    IEnumerator CustomUpdate() {
        print("antes");
        yield return new WaitForSeconds(2);
        print("después"); 
   }
   */

    IEnumerable CustomUpdate()
    {
        float originalX = transform.position.x;
        while(transform.position.x <= originalX +5)
        {
            transform.position = new Vector3(transform.position.x + Time.deltaTime, transform.position.y, transform.position.z);
            yield return new WaitForEndOfFrame();
        }
    }
}
