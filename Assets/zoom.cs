﻿using UnityEngine;
using System.Collections;

public class zoom : MonoBehaviour {

	public float zoomSize = 4f;
	public float smooth = 0.5f; 

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time >= 25) {
			Camera.main.fieldOfView = Mathf.Lerp (Camera.main.fieldOfView, zoomSize, Time.deltaTime * smooth);
		}
	}
}
