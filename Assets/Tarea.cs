﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tarea : MonoBehaviour {
    
    Renderer r;
	public Material[] materials;
    GameObject luz;
	 

	// Use this for initialization
	void Start () {
        r = GetComponent<Renderer>();

		//inicializar las coroutines 
		StartCoroutine (ChangeOne());
		StartCoroutine (ChangeTwo());
		StartCoroutine (ChangeThree());
		StartCoroutine (ChangeFour());
		StartCoroutine (ChangeFive());

        luz = GameObject.Find("Directional Light");

	}

    
	IEnumerator ChangeOne (){
		yield return new WaitForSeconds (5);
		r.material.shader = Shader.Find("Custom/CustomShader");
		float ambient = Mathf.PingPong(Time.time, 10);
		r.material.SetFloat("_AmbientColor", ambient);
		print ("cambie a custom shader");
        //luz.transform.Rotate(47, -372, -346);
	}

	IEnumerator ChangeThree (){
		yield return new WaitForSeconds (10); 
		r.material.shader = Shader.Find("Custom/NormalMapShader");
		print ("cambie a normal map shader");
	}

	IEnumerator ChangeFour (){
		yield return new WaitForSeconds (15);
		r.material.shader = Shader.Find ("Custom/ScrollingShader");
		print ("cambie a scrolling shader");
	}

	IEnumerator ChangeFive (){
		yield return new WaitForSeconds (25);
		r.material.shader = Shader.Find ("Custom/TextureShader");
		r.material = materials [1];
		print ("cambie a texture shader");
	}

	IEnumerator ChangeTwo (){
		yield return new WaitForSeconds (28);
		r.material.shader = Shader.Find ("Custom/ScrollingShader");
		r.material = materials [2];
		print ("cambie a scrolling shader");
	}

}
